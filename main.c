#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <wchar.h>

char *help_message = {
	"Usage:\n"
	"  txt2aes \"<text>\" - convert <text> to fullwidth text.\n"
	"  txt2aes help - display this help message.\n"
};

struct Commands {
	char *help;
};

struct Commands cmds = { "help" };
struct Commands *commands = &cmds;

wchar_t vaporwavify_char(char c) {
	wchar_t wide_c = c;
	switch(c) {
		case 'a': return L'ａ';
		case 'b': return L'ｂ';
		case 'c': return L'ｃ';
		case 'd': return L'ｄ';
		case 'e': return L'ｅ';
		case 'f': return L'ｆ';
		case 'g': return L'ｇ';
		case 'h': return L'ｈ';
		case 'i': return L'ｉ';
		case 'j': return L'ｊ';
		case 'k': return L'ｋ';
		case 'l': return L'ｌ';
		case 'm': return L'ｍ';
		case 'n': return L'ｎ';
		case 'o': return L'ｏ';
		case 'p': return L'ｐ';
		case 'q': return L'ｑ';
		case 'r': return L'ｒ';
		case 's': return L'ｓ';
		case 't': return L'ｔ';
		case 'u': return L'ｕ';
		case 'v': return L'ｖ';
		case 'w': return L'ｗ';
		case 'x': return L'ｘ';
		case 'y': return L'ｙ';
		case 'z': return L'ｚ';
		case 'A': return L'Ａ';
		case 'B': return L'Ｂ';
		case 'C': return L'Ｃ';
		case 'D': return L'Ｄ';
		case 'E': return L'Ｅ';
		case 'F': return L'Ｆ';
		case 'G': return L'Ｇ';
		case 'H': return L'Ｈ';
		case 'I': return L'Ｉ';
		case 'J': return L'Ｊ';
		case 'K': return L'Ｋ';
		case 'L': return L'Ｌ';
		case 'M': return L'Ｍ';
		case 'N': return L'Ｎ';
		case 'O': return L'Ｏ';
		case 'P': return L'Ｐ';
		case 'Q': return L'Ｑ';
		case 'R': return L'Ｒ';
		case 'S': return L'Ｓ';
		case 'T': return L'Ｔ';
		case 'U': return L'Ｕ';
		case 'V': return L'Ｖ';
		case 'W': return L'Ｗ';
		case 'X': return L'Ｘ';
		case 'Y': return L'Ｙ';
		case 'Z': return L'Ｚ';
		case '0': return L'０';
		case '1': return L'１';
		case '2': return L'２';
		case '3': return L'３';
		case '4': return L'４';
		case '5': return L'５';
		case '6': return L'６';
		case '7': return L'７';
		case '8': return L'８';
		case '9': return L'９';
		case ',': return L'，';
		case '.': return L'．';
		case ':': return L'：';
		case ';': return L'；';
		case '!': return L'！';
		case '?': return L'？';
		case '"': return L'＂';
		case '\'': return L'＇';
		case '`': return L'｀';
		case '^': return L'＾';
		case '~': return L'～';
		case '_': return L'＿';
		case '&': return L'＆';
		case '@': return L'＠';
		case '#': return L'＃';
		case '%': return L'％';
		case '+': return L'＋';
		case '-': return L'－';
		case '*': return L'＊';
		case '=': return L'＝';
		case '<': return L'＜';
		case '>': return L'＞';
		case '(': return L'（';
		case ')': return L'）';
		case '[': return L'［';
		case ']': return L'］';
		case '{': return L'｛';
		case '}': return L'｝';
		case '|': return L'｜';
		case '/': return L'／';
		case '\\': return L'＼';
		case ' ': return L'　';
		default: return wide_c;
	}
}

void vaporwaveify_str(char arg[]) {
	size_t arg_size = strlen(arg) + 1;
	wchar_t new_str_buff[arg_size];
	int i = 0;

	while (arg[i] != '\0') {
		new_str_buff[i] = vaporwavify_char(arg[i]);
		i++;
	}
	new_str_buff[i] = '\0';

	wprintf(L"%ls\n", new_str_buff);
}

int main(int argc, char *argv[]) {
	setlocale(LC_ALL, "C.UTF-8");
	switch(argc) {
		case 2:
			if (strcmp(argv[1], commands->help) == 0) {
				puts(help_message);
			}
			else {
				vaporwaveify_str(argv[1]);
			}
			break;
		default:
			puts(help_message);
			break;
	}
	return 0;
}
