# txt2aes-c

convert narrow-width text to wide-width text.

## compiling txt2aes-c

after compilation, a static binary called `txt2aes` is created in the current directory.

to compile txt2aes, run `make`.

## installing txt2aes-c

by default, txt2aes-c is installed in `~/.local/bin`.

to install txt2aes, run `make install`.

## installing txt2aes-c into a custom directory

set the `DEST` variable to a custom directory.

to install txt2aes-c into a customer directory, run `make install DEST=</path/to/directory>`.

for example, if you want to install txt2aes-c into `/usr/local/bin`, then run `make install DEST=/usr/local/bin`.

## usage

    ./txt2aes help
    => todo: i haven't made a help message yet lol

    ./txt2aes "hey there."
    => ｈｅｙ ｔｈｅｒｅ.

