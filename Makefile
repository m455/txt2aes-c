CC = cc
CFLAGS = -std=c99 \
	 -D_POSIX_C_SOURCE=200112 \
	 -pedantic \
	 -Wall \
	 -Werror \
	 -Wextra

PROG ?= txt2aes
SRC = ./main.c
DEST ?= ~/.local/bin

all: $(SRC)
	$(CC) $(CFLAGS) $(SRC) -o ./$(PROG)

install:
	install -Dm755 $(PROG) -D $(DEST)/$(PROG)

uninstall: $(DEST)/$(PROG)
	rm $(DEST)/$(PROG)

clean: ./$(PROG)
	rm $(PROG)

